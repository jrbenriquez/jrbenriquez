from core import views

from django.urls import include, re_path
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'bookmarks', views.BookmarkViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'posts', views.PostViewSet)
router.register(r'articles', views.ArticleViewSet)

urlpatterns = [
    re_path(r'^', include(router.urls)),
]
