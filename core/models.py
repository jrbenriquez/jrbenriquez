from django.db import models
from .utils import upload_bg

# Create your models here.


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    @property
    def readable_date(self):
        return self.created_at.strftime('%B %d, %Y')


class Category(models.Model):

    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Post(TimeStampedModel):

    title = models.CharField(max_length=64)
    category = models.ForeignKey(
        Category, related_name='bookmarks',
        on_delete=models.CASCADE)
    tags = models.TextField()
    background = models.FileField(upload_to=upload_bg, null=True)
    description = models.TextField(null=True, blank=True)


class Bookmark(Post):

    link = models.CharField(max_length=512)


class Article(Post):

    body = models.TextField()


class Image(Post):

    file = models.FileField(upload_to='images/')
    link = models.CharField(max_length=512)


class Video(Post):
    link = models.CharField(max_length=512)
    thumbnail = models.FileField(upload_to='thumbnail_vids/')
