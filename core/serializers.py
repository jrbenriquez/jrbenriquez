from rest_framework import serializers

from .models import (
    Bookmark, Category, Article, Image, Video, Post
)


class CustomSerializer(serializers.ModelSerializer):

    def get_field_names(self, declared_fields, info):
        expanded_fields = super(CustomSerializer, self).get_field_names(
            declared_fields, info)

        if getattr(self.Meta, 'extra_fields', None):
            return expanded_fields + self.Meta.extra_fields
        else:
            return expanded_fields


class BookmarkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bookmark
        fields = (
            'title', 'link', 'category',
            'created_at', 'modified_at', 'tags',
            'background'
        )


class ArticleSerializer(CustomSerializer):

    class Meta:
        model = Article
        fields = '__all__'
        extra_fields = ['readable_date']


class ImageSerializer(CustomSerializer):

    class Meta:
        model = Image
        fields = '__all__'
        extra_fields = ['readable_date']


class VideoSerializer(CustomSerializer):

    class Meta:
        model = Video
        fields = '__all__'
        extra_fields = ['readable_date']


class PostSerializer(CustomSerializer):

    class Meta:
        model = Post
        fields = '__all__'
        extra_fields = ['readable_date']


class CategorySerializer(serializers.ModelSerializer):

    bookmarks = BookmarkSerializer(many=True, read_only=True)
    posts = PostSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'bookmarks', 'posts')
        depth = 1
